import * as Yup from 'yup';

export const SubmitSchemas=Yup.object({
    name:Yup.string().min(2).max(25).required("Please Enter your name"),
    age:Yup.number().min(1).max(3).required("Please Enter your age"),
    gender:Yup.string().required("A radio option is required"),
    address:Yup.string().min(10).required("Please Enter your address"),
    phone:Yup.number().min(10).max(10).required("Please enter a valid Phone Number")

});

