
import './App.css';
// import {useState} from 'react';
import {useFormik} from 'formik';
import {SubmitSchemas} from './schemas/index';


function App() {

  // const [formData, setformData] = useState({
  //   'name':"",
  //   'age':"",
  //   'gender':"",
  //   'address':"",
  //   'phone':"",
  // });

  const initialValues={
      'name':"",
      'age':"",
      'gender':"",
      'address':"",
      'phone':"",
  }

  const {values,errors,touched,handleChange,handleBlur}=useFormik({
    initialValues:initialValues,
    validationSchema:SubmitSchemas,
    onSubmit:(values,action)=>{
      console.log(values);
      action.resetForm();
    },
  })


  // const onChangeInput=(e)=>{
  //   const formField=e.target.name;
  //   const  updateForm= {
  //     ...formData,
  //     [formField]: e.target.value,
  //   }
  //   console.log(e.target.name.value);
  //   setformData(updateForm);
  // }

  const  onSub=(event)=>{
    event.preventDefault();
    handleSubmit();
    // postsubmit();
  }


  const     handleSubmit  =async()=>{
    try{
      const requestOptions={
        method:'POST',
        headers:{ 'Content-type':'application/json' },
        body:JSON.stringify(values),
      }
      const res= await fetch('https://jsonplaceholder.typicode.com/users',requestOptions);
      const data=await res.json();
      
      console.log(data);
      
      
    }catch(err){
      console.log(err);
      
    }
    
  }


  return (
    <div className="App">
      <form onSubmit={onSub}>
        <table>

          <thead>
            <tr>
              <th colSpan="3"><span>Post : Form Data</span></th>
            </tr>
          </thead>

          <tbody>
              {/* Name of the Person */}
            <tr>
              <td>Name  </td>
              <td><input id="name" type="text" /*value={formData.name} onChange={onChangeInput}*/value={values.name} onChange={handleChange} onBlur={handleBlur} name="name" /></td>
              <td>{errors.name && touched.name ?<p className='form-error'>{errors.name}</p>:null}</td>
            </tr>
            
              {/* Age of the Person */}
            <tr>
              <td>Age  </td>
              <td><input id="age" type="number" /*value={formData.age} onChange={onChangeInput} */ value={values.age} onChange={handleChange} onBlur={handleBlur} name="age" /*maxLength="2"*//></td>
              <td>{errors.age && touched.age ?<p className='form-error'>{errors.age}</p>:null}</td>

            </tr>

              {/* Gender of the Person */}
              <tr>
              <td>Gender </td>
              <td>
                <input type="radio" id="male" name="gender" /*onChange={onChangeInput} */ onChange={handleChange} onBlur={handleBlur} value="Male"/>
                <label htmlFor="male">Male</label>
              <br/>
                <input type="radio" id="female" name="gender"  /*onChange={onChangeInput} */ onChange={handleChange} onBlur={handleBlur} value="Female"/>
                <label htmlFor="female">Female</label>
              <br/>
                <input type="radio" id="other" name="gender"  /*onChange={onChangeInput} */ onChange={handleChange} onBlur={handleBlur} value="Other"/>
                <label htmlFor="other">Other</label>
              </td>

              <br/>
              <td>{errors.gender && touched.gender ?<p className='form-error'>{errors.gender}</p>:null}</td>

            </tr>

              {/* Address of the Person */}
            <tr>
              <td>Address  </td>
              <td><textarea /*value={formData.address} onChange={onChangeInput} */ value={values.address} onChange={handleChange} onBlur={handleBlur} name='address' rows={5} cols={30}/></td>
              <td>{errors.address && touched.address ?<p className='form-error'>{errors.address}</p>:null}</td>
              
            </tr>
            
              {/* Phone No of the Person */}
            <tr>
              <td>Phone No  </td>
              <td><input id='phone' type="number" /*value={formData.phone} onChange={onChangeInput} */ value={values.phone} onChange={handleChange} onBlur={handleBlur} name="phone" max={10}/></td>
              <td>{errors.phone && touched.phone ?<p className='form-error'>{errors.phone}</p>:null}</td>
            </tr>

          </tbody>          
          
        </table>

        <button type='submit'>Submit</button>

      </form>
    </div>
  );
}

export default App;
